# Introduction

This project was made for temperature measurement. Several temperature sensors do regularly measurements and send it to an MQTT message broker (not contained in this project). The temperature recipient logs the temperature in a logfile, the analysis is done using Kibana 4.

As a simplified figure, the complete solution looks like this:

![temperature technical schema](documentation/technical_schema.png "Temperature technical schema")

The Java code just contains a project for the Raspberry Pi temperature sensor, the OpenWeatherMap sensor and the Temperature receiver. The MQTT Message Broker, Logstash and Kibana must be added somehow else.

Please have a look at the Readme.MD files of the Maven modules for further information.

# Requirements

To build and run all parts of this project you need

- installed [Lombok](https://projectlombok.org/) in your IDE
- Maven 3.2 or later
- Java 1.8 or later

## Compile the project

To compile, run the testcases and build the project just run

```bash
mvn clean package
```