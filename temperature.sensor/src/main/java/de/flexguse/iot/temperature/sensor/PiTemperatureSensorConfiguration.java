/**
 * 
 */
package de.flexguse.iot.temperature.sensor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.config.EnableIntegration;

import de.flexguse.iot.temperature.api.sensor.config.BaseSensorConfiguration;
import de.flexguse.iot.temperature.api.sensor.config.MqTTSenderConfiguration;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureService;
import de.flexguse.iot.temperature.sensor.service.PiTemperatureService;
import de.flexguse.iot.temperature.sensor.task.PushToMqTTTask;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Configuration
@Import(value={BaseSensorConfiguration.class, MqTTSenderConfiguration.class})
@EnableIntegration
public class PiTemperatureSensorConfiguration {
		
	@Scope("prototype")
	@Bean
	public Runnable pushToMqtt() {
		return new PushToMqTTTask();
	}

	@Bean
	public TemperatureService piTemperatureService() {
		return new PiTemperatureService();
	}


}
