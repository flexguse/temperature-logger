/**
 * 
 */
package de.flexguse.iot.temperature.sensor.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;

import de.flexguse.iot.temperature.api.sensor.service.TemperatureService;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureServiceException;
import de.flexguse.iot.temperature.sensor.integration.TemperatureInformationGateway;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class PushToMqTTTask implements Runnable {

	private Logger logger = LogManager.getLogger(getClass());

	@Autowired
	private TemperatureInformationGateway gateway;

	@Autowired
	private TemperatureService temperatureService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		try {
			gateway.processTemperatureInformation(MessageBuilder.withPayload(
					temperatureService.getTemperatureInformation()).build());
		} catch (TemperatureServiceException e) {
			logger.error("unable to read temperature information", e);
		}

	}

}
