package de.flexguse.iot.temperature.sensor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;

import de.flexguse.iot.temperature.api.sensor.config.MqTTRecipientConfiguration;

/**
 * Spring configuration for {@link TemperatureRecipient}.
 * <p>
 * The configuration class must extend {@link MqTTRecipientConfiguration}
 * instead of adding it to the @Import annotation to make Spring Integration run
 * correctly.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */

@SpringBootApplication
public class PiTemperatureSensor {

	@Qualifier("pushToMqtt")
	@Autowired
	private Runnable task;

	public static void main(String[] args) {
		SpringApplication.run(PiTemperatureSensor.class, args);
	}

	@Scheduled(cron = "${temperature.measurement.cron}")
	public void measureTemperature() {
		task.run();
	}
}
