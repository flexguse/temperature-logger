/**
 * 
 */
package de.flexguse.iot.temperature.sensor.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.regex.Pattern;

import lombok.Setter;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import de.flexguse.iot.temperature.api.converter.CelsiusToFahrenheitConverter;
import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureService;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureServiceException;

/**
 * This implementation uses Apache Commons Exec to read the temperature from a
 * Raspberry pi computer with connected DS1820 1-wire temperature sensor.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class PiTemperatureService implements TemperatureService {

	/**
	 * The path to do the linux CAT command and parse the result. Replace %s
	 * with the sensor number.
	 */
	private static final String QUERY_PATH = "cat /sys/bus/w1/devices/%s/w1_slave";

	/**
	 * The Apache Commons Exec executor which executes the command line command.
	 */
	@Setter
	private DefaultExecutor commandLineExecutor;

	@Setter
	@Autowired
	private CelsiusToFahrenheitConverter temperatureConverter;

	/**
	 * The hardware number of the sensor, needed to read the temperature.
	 */
	@Setter
	@Value("${sensor.number}")
	private String sensorNumber;

	/**
	 * The name of the room which is added to {@link TemperatureInformation}.
	 */
	@Setter
	@Value("${room.name}")
	private String roomName;

	private Pattern temperatureRegexPattern = Pattern.compile(
			".*t=[-]{0,1}[0-9]{4,5}.*", Pattern.MULTILINE | Pattern.DOTALL);

	public PiTemperatureService() {

		/*
		 * initialize the defaultExecutor
		 */
		commandLineExecutor = new DefaultExecutor();
		commandLineExecutor.setExitValue(0);

		// add watchdog to cancel if running command takes longer than 10
		// seconds
		ExecuteWatchdog watchdog = new ExecuteWatchdog(10000);
		commandLineExecutor.setWatchdog(watchdog);

	}

	@Override
	public TemperatureInformation getTemperatureInformation()
			throws TemperatureServiceException {

		CommandLine commandLine = CommandLine.parse(String.format(QUERY_PATH,
				sensorNumber));
		try {

			// prepare commandLineExecutor to be able to read console output
			ByteArrayOutputStream commandLineOutputStream = new ByteArrayOutputStream();
			PumpStreamHandler streamHandler = new PumpStreamHandler(
					commandLineOutputStream);
			commandLineExecutor.setStreamHandler(streamHandler);

			// call command line command
			int exitValue = commandLineExecutor.execute(commandLine);

			// get result and clean up
			String commandLineString = commandLineOutputStream.toString();
			streamHandler.stop();
			commandLineOutputStream.close();

			/*
			 * throw exception if exitValue is not 1
			 */
			if (exitValue != 0) {
				throw new TemperatureServiceException(
						String.format(
								"The command line command did not have the expected result but '%s', reading temperature failed",
								exitValue));
			}

			return parseCommandLineString(commandLineString, new Date());

		} catch (IOException e) {
			throw new TemperatureServiceException(
					"unable to do linux cat command to read temperature information",
					e);
		}
	}

	/**
	 * This helper method parses the commandLineString to
	 * {@link TemperatureInformation}. For testing purposes this method is
	 * public.
	 * 
	 * @param commandLineString
	 * @param currentDate
	 * @return
	 */
	public TemperatureInformation parseCommandLineString(
			String commandLineString, Date currentDate) {

		if (commandLineString != null
				&& temperatureRegexPattern.matcher(commandLineString).find()) {

			String temperature = commandLineString.substring(
					commandLineString.indexOf("t=") + 2,
					commandLineString.length());

			Double temperatureInCelsius = Double.valueOf(temperature) / 1000;

			return TemperatureInformation
					.builder()
					.degreesCelsius(temperatureInCelsius)
					.degreesFahrenheit(
							temperatureConverter.convert(temperatureInCelsius))
					.measureTimestamp(currentDate).room(roomName).build();

		}

		return null;

	}
}
