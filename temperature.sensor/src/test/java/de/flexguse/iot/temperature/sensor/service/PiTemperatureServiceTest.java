/**
 * 
 */
package de.flexguse.iot.temperature.sensor.service;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.iot.temperature.api.converter.CelsiusToFahrenheitConverter;
import de.flexguse.iot.temperature.api.model.TemperatureInformation;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class PiTemperatureServiceTest {

	private PiTemperatureService service;

	@Before
	public void setUp() {
		service = new PiTemperatureService();
		service.setTemperatureConverter(new CelsiusToFahrenheitConverter());
		service.setRoomName("test-room");
	}

	@Test
	public void testParseCommandLineNullInput() {

		assertNull("null command line input result in null",
				service.parseCommandLineString(null, null));

	}

	@Test
	public void testParseCommandLineValidInput() {

		/*
		 * prepare data
		 */
		LocalDateTime now = LocalDateTime.of(2015, Month.AUGUST, 4, 17, 57, 43);
		String commandLine = "32 00 4b 46 ff ff 0a 10 c1 : crc=c1 YES\n"
				+ "32 00 4b 46 ff ff 0a 10 c1 t=25125\n";

		/*
		 * call service
		 */
		TemperatureInformation info = service.parseCommandLineString(
				commandLine, Date.from(now.toInstant(ZoneOffset.UTC)));

		/*
		 * check expectations
		 */
		assertEquals("degrees in celsius", Double.valueOf(25.125),
				info.getDegreesCelsius());
		assertEquals("room name", "test-room", info.getRoom());
		assertNotNull("degrees in fahrenheit", info.getDegreesFahrenheit());

	}

	/**
	 * Tests if parsing does return null if t= is not in the command line.
	 */
	@Test
	public void testParseCommandLineMissingTemperatureInput() {

		/*
		 * prepare data
		 */
		LocalDateTime now = LocalDateTime.of(2015, Month.AUGUST, 4, 17, 57, 43);
		String commandLine = "some console output"
				+ "which does not contain any temperature information";

		/*
		 * call service
		 */
		TemperatureInformation info = service.parseCommandLineString(
				commandLine, Date.from(now.toInstant(ZoneOffset.UTC)));
		assertNull(info);

	}

}
