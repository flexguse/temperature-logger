# Introduction

This maven module is the implementation of a temperature sensor for the Raspberry Pi mini computer. This implementation connects to the MQTT message broker and in a configurable cron job the current temperature is sent.

Information about the required temperature sensor hardware and how it is configured can be found in the internet, [i.e. here](https://www.kompf.de/weather/pionewiremini.html).

Technically this temperature sensor is a Spring Boot application which uses Spring Integration to connect to the MQTT message broker. The temperature sensor is accessed by [Apache Commons Exec](https://commons.apache.org/proper/commons-exec/).

# Requirements

Please have a look at the [main requirements](../README.md) for the technical requirements of this maven module.

# Application configuration

The application configuration is done - as usual in Spring Boot - in the application.properties file.

| configuration property | description |
| ---------------------- | ------------|
| sensor.number			 | the hardware number of the DS1820 temperature sensor |
| room.name				 | the name of the room in which the temperature was taken, needed for later analysis |
| temperature.measurement.cron | the cron interval in which the temperature is taken, default value is every 5 minutes (0 0/5 * * * ?) |
| mqtt.username 	     | The name of the user which is used to connect to the MQTT message broker. For this project I used RabbitMQ, the default username is 'guest' which is configured as default. |
| mqtt.password   		 | The password for the MQTT message broker. Default value is 'guest'. |
| mqtt.uri				 | The URL to the MQTT message broker. Default value is 'tcp://localhost:1883'. |

# Configure as service

As any Spring-Boot application the temperature recipient can be easily configured as linux service, including memory settings. For further information please refer to the [Spring-Boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html).
 