# Introduction

This project contains all classes which could be used in a temperature sensor or a temperature receiver. The classes were designed to make the individual temperature sensor implementation as lean as possible.

# Requirements

Please have a look at the [main requirements](../README.md) for the technical requirements of this maven module.

# Packages

This description shall give an small overview of the packages contents.

## Package de.flexguse.iot.temperature.api.converter

In this package all temperature converters can be found which used in the temperature project.

## Package de.flexguse.iot.temperature.api.model

In this package the temperature data object can be found.

## Package de.flexguse.iot.temperature.api.sensor.config

Contains several Spring configuration classes which are commonly used in the temperature sensor implementations.

## Package de.flexguse.iot.temperature.api.sensor.service

Contains the service interfaces the temperature sensor implementations need to implement.

## Package de.flexguse.iot.temperature.sensor.integration

Commonly used Spring Integration classes which are needed by the temperature sensors to communicate with the MQTT message bus. 

