/**
 * 
 */
package de.flexguse.iot.temperature.api.converter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class FahrenheitToCelsiusConverterTest {

	private FahrenheitToCelsiusConverter converter = new FahrenheitToCelsiusConverter();

	@Test(expected = NullPointerException.class)
	public void testNullHandling() {
		converter.convert(null);
	}

	@Test
	public void testConversion() {

		assertEquals("102.20 °F", Double.valueOf(39.0),
				Double.valueOf(converter.convert(102.20)));
		assertEquals("33.8 °F", Double.valueOf(1),
				Double.valueOf(converter.convert(33.8)));
		assertEquals("116.6 °F", Double.valueOf(47),
				Double.valueOf(converter.convert(116.6)));
		assertEquals("389 °F", Double.valueOf(198.333),
				Double.valueOf(converter.convert(389.0)));
	}

}
