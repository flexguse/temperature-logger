/**
 * 
 */
package de.flexguse.iot.temperature.api.model;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.flexguse.util.junit.ValidationViolationChecker;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TemperatureInformationValidationTestConfiguration.class })
public class TemperatureInformationValidationTest {

	@Autowired
	private Validator validator;

	private ValidationViolationChecker<TemperatureInformation> checker = new ValidationViolationChecker<>();

	@Test
	public void testEmptyValidation() {

		Set<ConstraintViolation<TemperatureInformation>> validationErrors = validator
				.validate(TemperatureInformation.builder().build());

		checker.checkExpectedValidationViolations(
				validationErrors,
				Arrays.asList(
						TemperatureInformation.ERROR_VALIDATION_BLANK_ROOM,
						TemperatureInformation.ERROR_VALIDATION_MISSING_CELSIUS,
						TemperatureInformation.ERROR_VALIDATION_MISSING_TIMESTAMP));

	}

	@Test
	public void testMissingTimestamp() {

		Set<ConstraintViolation<TemperatureInformation>> validationErrors = validator
				.validate(TemperatureInformation.builder().build());

		checker.checkExpectedValidationViolations(
				validationErrors,
				Arrays.asList(
						TemperatureInformation.ERROR_VALIDATION_BLANK_ROOM,
						TemperatureInformation.ERROR_VALIDATION_MISSING_CELSIUS,
						TemperatureInformation.ERROR_VALIDATION_MISSING_TIMESTAMP));

	}

	@Test
	public void testValidTemperatureInformation() {
		Set<ConstraintViolation<TemperatureInformation>> validationErrors = validator
				.validate(TemperatureInformation.builder().degreesCelsius(20.5)
						.degreesFahrenheit(40.5).measureTimestamp(new Date())
						.room("office").build());

		checker.checkExpectedValidationViolations(validationErrors,
				Arrays.asList());
	}

	@Test
	public void testOutrangeCelsius() {

		Set<ConstraintViolation<TemperatureInformation>> validationErrors = validator
				.validate(TemperatureInformation.builder()
						.degreesCelsius(-120.5).degreesFahrenheit(40.5)
						.measureTimestamp(new Date()).room("office").build());

		checker.checkExpectedValidationViolations(
				validationErrors,
				Arrays.asList(TemperatureInformation.ERROR_VALIDATION_INVALID_CELSIUS));

	}

	@Test
	public void testOutrangeFahrenheit() {

		Set<ConstraintViolation<TemperatureInformation>> validationErrors = validator
				.validate(TemperatureInformation.builder()
						.degreesCelsius(-20.5).degreesFahrenheit(400.5)
						.measureTimestamp(new Date()).room("office").build());

		checker.checkExpectedValidationViolations(
				validationErrors,
				Arrays.asList(TemperatureInformation.ERROR_VALIDATION_INVALID_FAHRENHEIT));

	}

}
