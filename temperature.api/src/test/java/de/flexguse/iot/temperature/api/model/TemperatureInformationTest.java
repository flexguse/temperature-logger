/**
 * 
 */
package de.flexguse.iot.temperature.api.model;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Map;

import org.junit.Test;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class TemperatureInformationTest {

	/**
	 * Date must be formated properly
	 */
	@Test
	public void testContentAsMap() {

		Map<String, Object> contentMap = TemperatureInformation
				.builder()
				.degreesCelsius(17.9)
				.degreesFahrenheit(48.2)
				.room("testroom")
				.measureTimestamp(
						Date.from(LocalDateTime.of(2015, Month.AUGUST, 17, 16,
								42, 59).toInstant(ZoneOffset.UTC))).build()
				.contentsAsMap();

		assertEquals("testroom", contentMap.get("room"));
		assertEquals("degreesFahrenheit", 48.2,
				contentMap.get("degreesFahrenheit"));
		assertEquals("degreesCelsius", 17.9, contentMap.get("degreesCelsius"));
		assertEquals("measureTimestamp", "17.08.2015 18:42:59", contentMap.get("measureTimestamp"));

	}

}
