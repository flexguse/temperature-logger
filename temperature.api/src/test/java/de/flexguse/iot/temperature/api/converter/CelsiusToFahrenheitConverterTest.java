/**
 * 
 */
package de.flexguse.iot.temperature.api.converter;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class CelsiusToFahrenheitConverterTest {

	private CelsiusToFahrenheitConverter converter = new CelsiusToFahrenheitConverter();

	@Test(expected = NullPointerException.class)
	public void testNullHandling() {
		converter.convert(null);

	}

	@Test
	public void testConversion() {

		assertEquals("0 °C", Double.valueOf(32.0),
				Double.valueOf(converter.convert(0.0)));
		assertEquals("16 °C", Double.valueOf(60.8),
				Double.valueOf(converter.convert(16.0)));
		assertEquals("59 °C", Double.valueOf(138.2),
				Double.valueOf(converter.convert(59.0)));
		assertEquals("59.745 °C", Double.valueOf(139.541),
				Double.valueOf(converter.convert(59.745)));

		assertEquals("59.748 °C", Double.valueOf(139.546),
				Double.valueOf(converter.convert(59.748)));

	}

}
