/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.service;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;

/**
 * The {@link TemperatureService} gets the current temperature.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public interface TemperatureService {

	/**
	 * Gets the TemperatureInformation for the sensor implementation.
	 * 
	 * @return completely filled and valid {@link TemperatureInformation}
	 *         entity.
	 * @throws TemperatureServiceException
	 *             in case of technical problems
	 */
	public TemperatureInformation getTemperatureInformation()
			throws TemperatureServiceException;

}
