/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;

/**
 * The {@link TemperatureDispatcherService} sends the
 * {@link TemperatureInformation} to the recipient who does the data collecting.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Validated
public interface TemperatureDispatcherService {

	public static final String VALIDATION_ERROR_MISSING_TEMPERATURE_INFORMATION = "please give TemperatureInformation which can be dispatched";

	/**
	 * Sends the given {@link TemperatureInformation} to the recipient.
	 * 
	 * @param temperatureInformation
	 * @throws TemperatureDispatcherServiceException
	 */
	public void dispatchTemperatureInformation(
			@NotNull @Valid TemperatureInformation temperatureInformation)
			throws TemperatureDispatcherServiceException;

}
