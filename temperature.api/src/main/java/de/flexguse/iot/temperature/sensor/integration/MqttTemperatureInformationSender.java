/**
 * 
 */
package de.flexguse.iot.temperature.sensor.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.messaging.Message;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.config.MqTTSenderConfiguration;

/**
 * {@link MessageEndpoint} which listens for byte[] messages (containing
 * {@link TemperatureInformation} which needs to be sent to the MQTT message
 * broker.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@MessageEndpoint
public class MqttTemperatureInformationSender {

	/**
	 * This is the message handler which is able to send the message to the MQTT
	 * message broker.
	 */
	@Autowired
	private MqttPahoMessageHandler messageHandler;

	/**
	 * This method sends all messages containing byte[] payload to the MQTT
	 * message broker using the message handler.
	 * 
	 * @param byteMessage
	 */
	@ServiceActivator(inputChannel = MqTTSenderConfiguration.CHANNEL_OUTPUT_TEMPERATURE_BYTES)
	public void sendTemperatureToMqTT(Message<byte[]> byteMessage) {
		messageHandler.handleMessage(byteMessage);
	}

}
