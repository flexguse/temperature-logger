/**
 * 
 */
package de.flexguse.iot.temperature.api.converter;

import lombok.NonNull;

import org.springframework.core.convert.converter.Converter;

/**
 * This converter converts degrees in Celsius into degrees Fahrenheit. The
 * precision is 3 digits, everything else is rounded.
 * 
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class CelsiusToFahrenheitConverter implements Converter<Double, Double> {

	@Override
	public Double convert(@NonNull Double degreesInCelsius) {

		Double degreesInFahrenheit = (degreesInCelsius * 1.8) + 32;

		return Double.valueOf(Math.round(degreesInFahrenheit * 1000)) / 1000;
	}

}
