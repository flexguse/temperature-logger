/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.service.task;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureService;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureServiceException;

/**
 * The general TemperatureMeasurement task which just writes the
 * {@link TemperatureInformation} to System.out.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class TemperatureMeasureSysoutTask implements Runnable {

	@Autowired
	@Setter
	private TemperatureService temperatureService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		try {
			TemperatureInformation temperatureInformation = temperatureService.getTemperatureInformation();
					
			if(temperatureInformation != null){
				System.out.println(temperatureInformation);
			} else{
				System.out.println("measured 'null' TemperatureInformation");
			}
			
		} catch (TemperatureServiceException e) {
			e.printStackTrace();
		}

	}

}
