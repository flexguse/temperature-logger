/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.transformer.PayloadDeserializingTransformer;
import org.springframework.integration.transformer.PayloadSerializingTransformer;
import org.springframework.messaging.converter.MessageConverter;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;

/**
 * {@link TemperatureInformation}s can be sent to or received from the MQTT
 * message broker.
 * <p>
 * This abstract configuration class contains all common things the sender and
 * the receiver need to work properly.
 * </p>
 * <p>
 * If you want to implement a {@link TemperatureInformation} sensor which sends
 * the temperature, include {@link MqTTSenderConfiguration} to your Spring
 * configuration.
 * </p>
 * <p>
 * If you want to implement a {@link TemperatureInformation} recipient, include
 * {@link MqTTRecipientConfiguration} to your Spring configuration.
 * </p>
 * <p>
 * This separation shall ensure that a sensor - maybe with very limited hardware
 * resources - has no need to handle lots of {@link TemperatureInformation}
 * messages from the MQTT broker if a sensor always is sender and recipient.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public abstract class MqTTConfiguration {

	/**
	 * The name of the channel into which TemperatureInformation messages are
	 * put to be sent to the MQTT message broker.
	 */
	public static final String CHANNEL_OUTPUT_TEMPERATURE = "temperatureOutputChannel";

	/**
	 * The name of the channel into which the transformed TemperatureInformation
	 * are put.
	 */
	public static final String CHANNEL_OUTPUT_TEMPERATURE_BYTES = "byteTemperatureOutputChannel";

	/**
	 * The name of the channel into which the received TemperatureInformation
	 * messagres are put.
	 */
	public static final String CHANNEL_INPUT_TEMPERATURE = "temperature.input";

	/**
	 * The name of the channel into which the TemperatureInformation as byte[]
	 * are put.
	 */
	public static final String CHANNEL_INPUT_TEMPERATURE_BYTES = "temperature.input.byte";

	/**
	 * The name of the MQTT topic which is used to send and receive
	 * TemperatureInformation messages.
	 */
	protected static final String TOPIC_TEMPERATURE = "de/flexguse/iot/temperature";

	/**
	 * The username which is used to connect to the MQTT message broker. The
	 * username can be configured in application.properties by setting
	 * mqtt.username. Default value is 'guest'.
	 */
	@Value("${mqtt.username:guest}")
	protected String username;

	/**
	 * The password which is used to connect to the MQTT message broker. The
	 * password can be configured in application.properties by setting
	 * mqtt.password. Default value is 'guest'.
	 */
	@Value("${mqtt.password:guest}")
	protected String password;

	/**
	 * The URI to the MQTT message broker. The URI can be configrued in
	 * application.properties by setting 'mqtt.uri'. Default value is
	 * 'rcp://localhost:1883'.
	 */
	@Value("${mqtt.uri:tcp://localhost:1883}")
	protected String mqttBrokerURI;

	/**
	 * The factory which is used to connect to the MQTT broker. Eclipse Paho is
	 * used under the hood.
	 * 
	 * @return
	 */
	@Bean
	public MqttPahoClientFactory mqttPahoClientFactory() {

		DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
		factory.setUserName(username);
		factory.setPassword(password);
		factory.setServerURIs(new String[]{mqttBrokerURI});

		return factory;
	}

	/**
	 * The {@link MessageConverter} which converts Spring Integration messages
	 * to MQTT messages.
	 * <p>
	 * As we put byte[] as message payload it is essential to setPayloadAsBytes
	 * to true, otherwise the deserialization does not work.
	 * </p>
	 * 
	 * @return
	 */
	@Bean
	public DefaultPahoMessageConverter pahoMessageConverter() {

		DefaultPahoMessageConverter converter = new DefaultPahoMessageConverter();
		converter.setPayloadAsBytes(true);
		return converter;

	}

	/**
	 * This payload transformer is used to transform
	 * {@link TemperatureInformation} into byte[] array.
	 * 
	 * @return
	 */
	@Bean
	public PayloadSerializingTransformer payloadToBytesSerializer() {
		return new PayloadSerializingTransformer();
	}

	/**
	 * This paylod transformer is used to transform byte[] to
	 * {@link TemperatureInformation}.
	 * 
	 * @return
	 */
	@Bean
	public PayloadDeserializingTransformer bytesToPayloadSerializer() {
		return new PayloadDeserializingTransformer();
	}

}
