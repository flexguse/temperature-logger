/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.messaging.MessageChannel;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;

/**
 * Spring Integration configuration class for temperature recipients.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@EnableIntegration
public class MqTTRecipientConfiguration extends MqTTConfiguration {

	@Scope("singleton")
	@Bean(name = CHANNEL_INPUT_TEMPERATURE)
	public MessageChannel temperatureChannel() {
		return MessageChannels.direct(CHANNEL_INPUT_TEMPERATURE)
				.datatype(TemperatureInformation.class).get();
	}

	@Scope("singleton")
	@Bean(name = CHANNEL_INPUT_TEMPERATURE_BYTES)
	public MessageChannel temperatureBytesChannel() {
		return MessageChannels.direct(CHANNEL_INPUT_TEMPERATURE_BYTES).get();
	}

	/**
	 * This is the message flow for receiving {@link TemperatureInformation}
	 * from the MQTT message broker.
	 * <p>
	 * Messages are received by temperatureInput()
	 * {@link MqttPahoMessageDrivenChannelAdapter} and put into
	 * CHANNEL_INPUT_TEMPERATURE_BYTES, transformed into
	 * {@link TemperatureInformation} messages and put into
	 * CHANNEL_INPUT_TEMPERATURE.
	 * </p>
	 * The listener for CHANNEL_INPUT_TEMPERATURE is defined as
	 * {@link MessageEndpoint}.
	 * 
	 * @return
	 */
	@Bean
	public IntegrationFlow temperatureInputIntegrationFlow() {

		return IntegrationFlows.from(CHANNEL_INPUT_TEMPERATURE_BYTES)
				.transform(bytesToPayloadSerializer())
				.channel(CHANNEL_INPUT_TEMPERATURE).get();

	}

	/**
	 * The channel adapter which receives {@link TemperatureInformation}
	 * messages from the MQTT message broker.
	 * 
	 * @return
	 */
	@Bean
	public MqttPahoMessageDrivenChannelAdapter temperatureInput() {

		MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter(
				mqttBrokerURI, "temperatureInformation-receiver",
				mqttPahoClientFactory(), TOPIC_TEMPERATURE);
		adapter.setAutoStartup(true);
		adapter.setConverter(pahoMessageConverter());
		adapter.setOutputChannel(temperatureBytesChannel());

		return adapter;

	}

}
