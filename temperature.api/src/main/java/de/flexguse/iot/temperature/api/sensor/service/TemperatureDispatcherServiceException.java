/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.service;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class TemperatureDispatcherServiceException extends Exception {

	private static final long serialVersionUID = 4511981603660361231L;

}
