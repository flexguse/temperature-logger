/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.sensor.integration.MqttTemperatureInformationSender;

/**
 * This configuration configures Spring Integration to set up some message
 * channels and message flows.
 * <p>
 * There are two message channel flows:
 * <ol>
 * <li>CHANNEL_OUTPUT_TEMPERATURE -> CHANNEL_OUTPUT_TEMPERATURE_BYTES: this flow
 * is for sending TemperatureInformations to the MQTT broker</li>
 * <li>CHANNEL_INPUT_TEMPERATURE_BYTES -> CHANNEL_INPUT_TEMPERATURE: this flow
 * is for receiving TemperatureInformations from the MQTT broker</li>
 * </ol>
 * </p>
 * The configuration of the message channel flows is done using Spring
 * Integration DSL
 * (https://github.com/spring-projects/spring-integration-java-dsl
 * /wiki/Spring-Integration-Java-DSL-Reference) to avoid XML configuration.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Configuration
@IntegrationComponentScan("de.flexguse.iot.temperature.sensor.integration")
public class MqTTSenderConfiguration extends MqTTConfiguration {

	@Value("${mqtt.sender.id:temperatureInformation-sender}")
	private String senderID;
	
	/**
	 * The message channel into which {@link TemperatureInformation}s are put
	 * into.
	 * 
	 * @return
	 */
	@Bean(name = CHANNEL_OUTPUT_TEMPERATURE)
	public MessageChannel temperatureMessageOutputChannel() {

		return MessageChannels.direct(CHANNEL_OUTPUT_TEMPERATURE)
				.datatype(TemperatureInformation.class).get();
	}

	/**
	 * The message channel into which {@link TemperatureInformation}s are put
	 * into after they were serialized to byte[].
	 * 
	 * @return
	 */
	@Bean(name = CHANNEL_OUTPUT_TEMPERATURE_BYTES)
	public MessageChannel byteTemperatureMessageOutputChannel() {
		return MessageChannels.direct(CHANNEL_OUTPUT_TEMPERATURE_BYTES).get();
	}

	/**
	 * This is the message flow for sending {@link TemperatureInformation} to
	 * the MQTT message broker.
	 * <p>
	 * Messages are taken from CHANNEL_INPUT_TEMPERATURE, are transformed to
	 * byte[] messages and are put into CHANNEL_INPUT_TEMPERATURE_BYTES.
	 * </p>
	 * The listener for CHANNEL_INPUT_TEMPERATURE_BYTES is defined in
	 * {@link MqttTemperatureInformationSender}.
	 * 
	 * @return
	 */
	@Bean
	public IntegrationFlow temperatureIntegrationOutputFlow() {

		return IntegrationFlows.from(CHANNEL_OUTPUT_TEMPERATURE)
				.transform(payloadToBytesSerializer())
				.channel(CHANNEL_OUTPUT_TEMPERATURE_BYTES).get();

	}

	/**
	 * The {@link MessageHandler} which connects to the MQTT message broker and
	 * sends the message.
	 * 
	 * @return
	 */
	@Bean
	public MqttPahoMessageHandler temperatureOutput() {
		MqttPahoMessageHandler handler = new MqttPahoMessageHandler(
				mqttBrokerURI, senderID,
				mqttPahoClientFactory());

		handler.setAsync(true);
		handler.setConverter(pahoMessageConverter());
		handler.setDefaultQos(2);
		handler.setDefaultTopic(TOPIC_TEMPERATURE);
		return handler;
	}

}
