/**
 * 
 */
package de.flexguse.iot.temperature.api.converter;

import lombok.NonNull;

import org.springframework.core.convert.converter.Converter;

/**
 * This converter converts degrees in Fahrenheit into degrees celsius. The
 * precision is 3 digits, everything else is rounded.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class FahrenheitToCelsiusConverter implements Converter<Double, Double> {

	@Override
	public Double convert(@NonNull Double degreesInFahrenheit) {

		Double degreesInCelsius = (degreesInFahrenheit - 32) / 1.8;

		return Double.valueOf(Math.round(degreesInCelsius * 1000)) / 1000;
	}

}
