/**
 * 
 */
package de.flexguse.iot.temperature.api.sensor.service;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class TemperatureServiceException extends Exception {

	private static final long serialVersionUID = -4181503360589272270L;

	public TemperatureServiceException(String message, Exception e) {
		super(message, e);
	}
	
	public TemperatureServiceException(String message){
		super(message);
	}

}
