/**
 * 
 */
package de.flexguse.iot.temperature.sensor.integration;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.config.MqTTSenderConfiguration;

/**
 * The {@link MessagingGateway} for which the implementation is generated
 * automatically by Spring Integration.
 * <p>
 * The {@link MessagingGateway} connects the application logic with the Spring
 * Integration message channels.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@MessagingGateway(name = "temperatureInformationGateway")
public interface TemperatureInformationGateway {

	/**
	 * Every message containing {@link TemperatureInformation} is set to the
	 * CHANNEL_OUTPUT_TEMPERATURE channel.
	 * 
	 * @param temperatureInformationMessage
	 */
	@Gateway(requestChannel = MqTTSenderConfiguration.CHANNEL_OUTPUT_TEMPERATURE)
	public void processTemperatureInformation(
			Message<TemperatureInformation> temperatureInformationMessage);

}
