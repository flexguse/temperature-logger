/**
 * 
 */
package de.flexguse.iot.temperature.api.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 * This is the temperature information entity which is sent from the temperature
 * client to the temperature recipient.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TemperatureInformation implements Serializable {

	private static final long serialVersionUID = 4306324837002142284L;

	public static final String ERROR_VALIDATION_MISSING_TIMESTAMP = "Measure timestamp is required";

	public static final String ERROR_VALIDATION_MISSING_CELSIUS = "at least the degrees in Celsius must be given";

	public static final String ERROR_VALIDATION_BLANK_ROOM = "Room must be set";

	public static final String ERROR_VALIDATION_INVALID_CELSIUS = "The degrees in celsius must be between -40 and 120 °Celsius";

	public static final String ERROR_VALIDATION_INVALID_FAHRENHEIT = "The degrees in celsius must be between -40 and 248 °Fahrenheit";

	/**
	 * Name of the room in which the temperature was measured.
	 */
	@NotBlank(message = ERROR_VALIDATION_BLANK_ROOM)
	private String room;

	/**
	 * The measured temperature in degrees Celsius.
	 */
	@NotNull(message = ERROR_VALIDATION_MISSING_CELSIUS)
	@Range(min = -40, max = 120, message = ERROR_VALIDATION_INVALID_CELSIUS)
	private Double degreesCelsius;

	/**
	 * The measured temperature in degrees Farenheit.
	 */
	@Range(min = -40, max = 248, message = ERROR_VALIDATION_INVALID_FAHRENHEIT)
	private Double degreesFahrenheit;

	/**
	 * The timestamp
	 */
	@NotNull(message = ERROR_VALIDATION_MISSING_TIMESTAMP)
	private Date measureTimestamp;

	/**
	 * Convenience method which gets the content of this entity as Map. Key is
	 * the propertyName, Value is the property value.
	 * 
	 * @return
	 */
	public Map<String, Object> contentsAsMap() {

		Map<String, Object> result = new HashMap<>();
		result.put("room", this.room);
		result.put("degreesCelsius", this.degreesCelsius);
		result.put("degreesFahrenheit", this.degreesFahrenheit);
		result.put(
				"measureTimestamp",
				DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
						DateFormat.MEDIUM, Locale.GERMAN).format(
						this.measureTimestamp));

		return result;

	}

}
