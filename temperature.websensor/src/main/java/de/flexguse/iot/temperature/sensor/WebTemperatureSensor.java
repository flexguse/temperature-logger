package de.flexguse.iot.temperature.sensor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
public class WebTemperatureSensor {

	@Qualifier("pushToMqtt")
	@Autowired
	private Runnable task;
	
    public static void main(String[] args) {
        SpringApplication.run(WebTemperatureSensor.class, args);
    }
    
	@Scheduled(cron = "${temperature.measurement.cron}")
	public void measureTemperature() {
		task.run();
	}
}
