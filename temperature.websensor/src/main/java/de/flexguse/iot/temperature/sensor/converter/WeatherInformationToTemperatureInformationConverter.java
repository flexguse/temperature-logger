/**
 * 
 */
package de.flexguse.iot.temperature.sensor.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import lombok.NonNull;
import lombok.Setter;

import org.springframework.core.convert.converter.Converter;

import de.flexguse.iot.temperature.api.converter.CelsiusToFahrenheitConverter;
import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.sensor.model.openweathermap.WeatherInformation;

/**
 * This converter converts {@link WeatherInformation} from OpenWeatherMap into
 * {@link TemperatureInformation}.
 * <p>
 * To create a valid {@link TemperatureInformation} the room information must be
 * set.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class WeatherInformationToTemperatureInformationConverter implements
		Converter<WeatherInformation, TemperatureInformation> {

	@Setter
	private String room;

	private CelsiusToFahrenheitConverter converter = new CelsiusToFahrenheitConverter();

	@Override
	public TemperatureInformation convert(
			@NonNull WeatherInformation weatherInformation) {

		Double temperatureInCelsius = Double
				.valueOf(Math
						.round((weatherInformation.getMain().getTemp() - 273.15) * 1000)) / 1000.0;

		return TemperatureInformation
				.builder()
				.degreesFahrenheit(converter.convert(temperatureInCelsius))
				.degreesCelsius(temperatureInCelsius)
				.room(room)
				.measureTimestamp(
						Date.from(LocalDateTime.now()
								.atZone(ZoneId.systemDefault()).toInstant()))
				.build();
	}

}
