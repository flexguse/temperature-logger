package de.flexguse.iot.temperature.sensor.model.openweathermap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class MainWeather {

	/**
	 * Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 */
	private Double temp;

	/**
	 * Atmospheric pressure (on the sea level, if there is no sea_level or
	 * grnd_level data), hPa
	 */
	private Double pressure;

	/**
	 * Humidity, %
	 */
	private Double humidity;

	/**
	 * Minimum temperature at the moment. This is deviation from current temp
	 * that is possible for large cities and megalopolises geographically
	 * expanded (use these parameter optionally). Unit Default: Kelvin, Metric:
	 * Celsius, Imperial: Fahrenheit.
	 */
	private Double temp_min;

	/**
	 * Maximum temperature at the moment. This is deviation from current temp
	 * that is possible for large cities and megalopolises geographically
	 * expanded (use these parameter optionally). Unit Default: Kelvin, Metric:
	 * Celsius, Imperial: Fahrenheit.
	 */
	private Double temp_max;

	/**
	 * Atmospheric pressure on the sea level, hPa
	 */
	@JsonProperty("sea_level")
	private Double seaLevel;

	/**
	 * Atmospheric pressure on the ground level, hPa
	 */
	@JsonProperty("grnd_level")
	private Double groundLevel;

}
