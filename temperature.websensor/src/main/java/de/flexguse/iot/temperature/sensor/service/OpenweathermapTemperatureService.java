/**
 * 
 */
package de.flexguse.iot.temperature.sensor.service;

import java.util.HashMap;
import java.util.Map;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureService;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureServiceException;
import de.flexguse.iot.temperature.sensor.converter.WeatherInformationToTemperatureInformationConverter;
import de.flexguse.iot.temperature.sensor.model.openweathermap.WeatherInformation;

/**
 * This implementation uses OpenWeatherMap (http://www.openweathermap.org) to
 * get the current outside temperature information.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class OpenweathermapTemperatureService implements TemperatureService {

	private static final String SERVICE_URL = "http://api.openweathermap.org/data/2.5/weather?q={zip},{country}&appid={apiKey}";
	
	@Setter
	@Value("${api.key}")
	private String apiKey;

	/**
	 * The ZIP code of the city for which the temperature shall be fetched.
	 */
	@Setter
	private String zipCode;

	/**
	 * ISO 3166 country code (i.e. 'DE' or 'US') for the country which is used
	 * to resolve the zipCode.
	 */
	@Setter
	private String countryCode;

	/**
	 * The {@link RestTemplate} which is used to call the service url.
	 */
	@Autowired
	@Setter
	private RestTemplate restTemplate;

	/**
	 * The converter which does the {@link WeatherInformation} to
	 * {@link TemperatureInformation} conversion.
	 */
	@Setter
	@Autowired
	private WeatherInformationToTemperatureInformationConverter converter;

	@Override
	public TemperatureInformation getTemperatureInformation()
			throws TemperatureServiceException {

		try {
			
			Map<String, Object> urlPropertyMap = new HashMap<>();
			urlPropertyMap.put("zip", zipCode);
			urlPropertyMap.put("country", countryCode.toLowerCase());
			urlPropertyMap.put("apiKey", apiKey);
			
			// call OpenWeatherMap
			ResponseEntity<WeatherInformation> response = restTemplate
					.getForEntity(SERVICE_URL,
							WeatherInformation.class, urlPropertyMap);

			// convert and return result
			return converter.convert(response.getBody());

		} catch (RestClientException e) {
			throw new TemperatureServiceException(
					"unable to get temperature information from OpenWeatherMap",
					e);
		}

	}

}
