/**
 * 
 */
package de.flexguse.iot.temperature.sensor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;

import de.flexguse.iot.temperature.api.sensor.config.BaseSensorConfiguration;
import de.flexguse.iot.temperature.api.sensor.config.MqTTSenderConfiguration;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureService;
import de.flexguse.iot.temperature.sensor.converter.WeatherInformationToTemperatureInformationConverter;
import de.flexguse.iot.temperature.sensor.service.OpenweathermapTemperatureService;
import de.flexguse.iot.temperature.sensor.task.PushToMqTTTask;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Configuration
@Import(BaseSensorConfiguration.class)
public class WebTemperatureSensorConfiguration extends MqTTSenderConfiguration {

	@Value("${openweathermap.zipcode:57439}")
	private String zipCode;

	@Value("${openweathermap.countrycode:de}")
	private String countryCode;

	@Scope("prototype")
	@Bean
	public Runnable pushToMqtt() {
		return new PushToMqTTTask();
	}

	@Bean
	public WeatherInformationToTemperatureInformationConverter converter() {

		WeatherInformationToTemperatureInformationConverter converter = new WeatherInformationToTemperatureInformationConverter();
		converter.setRoom("Attendorn");

		return converter;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public TemperatureService temperatureService() {
		OpenweathermapTemperatureService service = new OpenweathermapTemperatureService();
		service.setZipCode(zipCode);
		service.setCountryCode(countryCode);

		return service;
	}

}
