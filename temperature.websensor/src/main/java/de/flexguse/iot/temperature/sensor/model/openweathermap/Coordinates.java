/**
 * 
 */
package de.flexguse.iot.temperature.sensor.model.openweathermap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class Coordinates {

	@JsonProperty(value = "lon")
	private Double longiture;

	@JsonProperty(value = "lat")
	private Double latitude;

}
