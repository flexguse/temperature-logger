/**
 * 
 */
package de.flexguse.iot.temperature.sensor.model.openweathermap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class WeatherInformation {

	@JsonProperty("coord")
	private Coordinates coordinates;

	private String base;

	@JsonProperty("dt")
	private Long dataTimestamp;

	private MainWeather main;

}
