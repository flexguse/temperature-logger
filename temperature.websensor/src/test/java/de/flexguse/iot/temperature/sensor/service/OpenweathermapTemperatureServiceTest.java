/**
 * 
 */
package de.flexguse.iot.temperature.sensor.service;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.service.TemperatureServiceException;
import de.flexguse.iot.temperature.sensor.converter.WeatherInformationToTemperatureInformationConverter;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class OpenweathermapTemperatureServiceTest {

	private OpenweathermapTemperatureService openweathermapService;

	MockRestServiceServer mockServer;

	@Before
	public void setUp() {

		openweathermapService = new OpenweathermapTemperatureService();
		openweathermapService.setCountryCode("DE");
		openweathermapService.setZipCode("57439");
		openweathermapService.setApiKey("12345");
		
		RestTemplate restTemplate = new RestTemplate();
		mockServer = MockRestServiceServer.createServer(restTemplate);

		mockServer
				.expect(requestTo("http://api.openweathermap.org/data/2.5/weather?q=57439,de&appid=12345"))
				.andRespond(
						withSuccess(
								readFile("/openweathermap/temperature.json"),
								MediaType.APPLICATION_JSON));

		openweathermapService.setRestTemplate(restTemplate);

		WeatherInformationToTemperatureInformationConverter converter = new WeatherInformationToTemperatureInformationConverter();
		converter.setRoom("test-room");
		openweathermapService.setConverter(converter);

	}

	/**
	 * Helper method reads the given file into string.
	 * 
	 * @param pathToFile
	 *            relative to src/test/resources
	 * @return
	 */
	private String readFile(String pathToFile) {

		Scanner scanner = new Scanner(getClass()
				.getResourceAsStream(pathToFile));

		StringBuilder fileContent = new StringBuilder();
		while (scanner.hasNext()) {
			fileContent.append(scanner.next());
		}

		scanner.close();

		return fileContent.toString();

	}

	@Test
	public void testGetTemperatureInformation()
			throws TemperatureServiceException {

		TemperatureInformation information = openweathermapService
				.getTemperatureInformation();

		// check if mockServer was called
		mockServer.verify();

		assertEquals("test-room", information.getRoom());
		assertEquals(Double.valueOf(15.98), information.getDegreesCelsius());
		assertEquals(Double.valueOf(60.764), information.getDegreesFahrenheit());
//		assertEquals(
//				Date.from(LocalDateTime.of(2015, Month.JULY, 30, 18, 9, 39)
//						.atZone(ZoneId.systemDefault()).toInstant()),
//				information.getMeasureTimestamp());
	}

}
