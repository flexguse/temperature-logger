/**
 * 
 */
package de.flexguse.iot.temperature.sensor.converter;

import static org.junit.Assert.assertEquals;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.sensor.model.openweathermap.Coordinates;
import de.flexguse.iot.temperature.sensor.model.openweathermap.MainWeather;
import de.flexguse.iot.temperature.sensor.model.openweathermap.WeatherInformation;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class WeatherInformationToTemperatureInformationConverterTest {

	private WeatherInformationToTemperatureInformationConverter converter;

	@Before
	public void setUp() {

		converter = new WeatherInformationToTemperatureInformationConverter();
		converter.setRoom("test-room");

	}

	@Test(expected = NullPointerException.class)
	public void testNullHandling() {

		converter.convert(null);

	}

	/**
	 * This testcase tests the conversion. It is assumed OpenWeatherMap always
	 * delivers valid information so it is not checked what happens in case of
	 * i.e. missing {@link MainWeather}.
	 */
	@Test
	public void testConversion() {

		Instant now = Instant.now();
		LocalDateTime localDateTime = LocalDateTime.ofInstant(now,
				ZoneId.of("UTC"));
		localDateTime = localDateTime.withNano(0);

		// create WeatherInformation
		WeatherInformation info = WeatherInformation
				.builder()
				.base("testcase")
				.coordinates(
						Coordinates.builder().latitude(20.0).longiture(20.0)
								.build())
				.dataTimestamp(localDateTime.toEpochSecond(ZoneOffset.UTC))
				.main(MainWeather.builder().temp(292.68).build()).build();

		// do conversion
		TemperatureInformation temperatureInformation = converter.convert(info);

		// check expectations
		assertEquals("test-room", temperatureInformation.getRoom());
		assertEquals("° Celsius", Double.valueOf(19.53),
				temperatureInformation.getDegreesCelsius());
		assertEquals("° Fahrenheit", Double.valueOf(67.154),
				temperatureInformation.getDegreesFahrenheit());

//		assertEquals(localDateTime, LocalDateTime.ofInstant(
//				temperatureInformation.getMeasureTimestamp().toInstant(),
//				ZoneId.of("UTC")));

	}

}
