# Introduction

This maven module is the implementation of a temperature sensor for the [OpenWeatherMap](http://openweathermap.org/) webservice. This implementation connects to the MQTT message broker and in a configurable cron job the current temperature is sent.

Technically this temperature sensor is a Spring Boot application which uses Spring Integration to connect to the MQTT message broker. The external webservice is accessed by using Spring RestTemplate.

# Requirements

Please have a look at the [main requirements](../README.md) for the technical requirements of this maven module.

# Application configuration

The application configuration is done - as usual in Spring Boot - in the application.properties file.

| configuration property | description |
| ---------------------- | ------------|
| mqtt.sender.id		 | the ID of the sensor, default value is openweathermap-sensor |
| temperature.measurement.cron | the cron interval in which the temperature is taken, default value is every 15 minutes (0 0/15 * * * ?) |
| api.key				 | the API key for the OpenWeatherMap webservice you got after creating an account |
| mqtt.username 	     | The name of the user which is used to connect to the MQTT message broker. For this project I used RabbitMQ, the default username is 'guest' which is configured as default. |
| mqtt.password   		 | The password for the MQTT message broker. Default value is 'guest'. |
| mqtt.uri				 | The URL to the MQTT message broker. Default value is 'tcp://localhost:1883'. |

# Configure as service

As any Spring-Boot application the temperature recipient can be easily configured as linux service, including memory settings. For further information please refer to the [Spring-Boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html).
 