/**
 * 
 */
package de.flexguse.iot.temperature.recipient.integration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

import de.flexguse.iot.temperature.api.model.TemperatureInformation;
import de.flexguse.iot.temperature.api.sensor.config.MqTTRecipientConfiguration;

/**
 * The {@link MessageEndpoint} which listens to incoming
 * {@link TemperatureInformation}s and does the {@link TemperatureInformation}
 * logging via Log4j2.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@MessageEndpoint
public class MqttTemperatureInformationLogger {

	private static Logger logger = LogManager
			.getLogger(MqttTemperatureInformationLogger.class);

	/**
	 * Does the logging. The binding to the channel is done by adding the
	 * {@link ServiceActivator} annotation and giving the inputChannel the
	 * method shall listen to.
	 * 
	 * @param message
	 */
	@ServiceActivator(inputChannel = MqTTRecipientConfiguration.CHANNEL_INPUT_TEMPERATURE)
	public void handleTemperatureMessage(Message<TemperatureInformation> message) {

		logger.info(message.getPayload().contentsAsMap());

	}

}
