/**
 * 
 */
package de.flexguse.iot.temperature.recipient;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

import de.flexguse.iot.temperature.api.sensor.config.BaseSensorConfiguration;
import de.flexguse.iot.temperature.api.sensor.config.MqTTRecipientConfiguration;

/**
 * Spring configuration for {@link TemperatureRecipient}.
 * <p>
 * The configuration class must extend {@link MqTTRecipientConfiguration}
 * instead of adding it to the @Import annotation to make Spring Integration run
 * correctly.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Configuration
@Import({ BaseSensorConfiguration.class })
@EnableIntegration
@IntegrationComponentScan("de.flexguse.iot.temperature.recipient.integration")
public class TemperatureRecipientConfiguration extends
		MqTTRecipientConfiguration {

}
