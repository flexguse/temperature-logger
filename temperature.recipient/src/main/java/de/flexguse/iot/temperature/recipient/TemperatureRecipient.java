package de.flexguse.iot.temperature.recipient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot application for the temperature sensor implementation designed
 * for a Raspberry Pi computer with an attached DS18S20 temperature sensor.
 * 
 * @author Christoph Guse, info@flexguse.de
 *
 */
@SpringBootApplication
public class TemperatureRecipient {

	public static void main(String[] args) {
		SpringApplication.run(TemperatureRecipient.class, args);
	}
}
