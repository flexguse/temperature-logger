# Introduction

This maven module is the implementation of a temperature recipient. This implementation connects to the MQTT message broker and as soon a temperature information is dispatched by the message broker this implementation logs the TemperatureInformation to an logfile.

Technically this temperature recipient is a Spring Boot application which uses Spring Integration to connect to the MQTT message broker.

# Requirements

Please have a look at the [main requirements](../README.md) for the technical requirements of this maven module.

# Application configuration

The application configuration is done - as usual in Spring Boot - in the application.properties file.

| configuration property | description |
| ---------------------- | ------------|
| mqtt.username 	     | The name of the user which is used to connect to the MQTT message broker. For this project I used RabbitMQ, the default username is 'guest' which is configured as default. |
| mqtt.password   		 | The password for the MQTT message broker. Default value is 'guest'. |
| mqtt.uri				 | The URL to the MQTT message broker. Default value is 'tcp://localhost:1883'. |

# Configure as service

As any Spring-Boot application the temperature recipient can be easily configured as linux service, including memory settings. For further information please refer to the [Spring-Boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html).
 